package com.sutsuj.GUI;

import android.app.*;
import android.os.*;
import android.view.*;
import android.widget.*;
import android.content.*;
import com.sutsuj.ioHandler.*;
import com.sutsuj.ioHandler.parser.InputParser;
import com.sutsuj.ioHandler.parser.ProgrammChoserParser;

public class MainActivity extends Activity
{
	
	//We use a Parser for different programms, which can be chosen
	private InputParser parser;
	
    /** Called when the activity is first created. */
    @Override
    public void onCreate(Bundle savedInstanceState)
	{
        super.onCreate(savedInstanceState);

		//Create a gui
        setContentView(R.layout.main);

		//Get pointer to the elements
		TextView output = (TextView)findViewById(R.id.textOutput);
		EditText input = (EditText)findViewById(R.id.textInput);

		//Get an Parser
		ParseIoHandler io = new ParseIoHandler(this, output, input, null);
		//... and set the Parser, we use
		parser = new ProgrammChoserParser(io);
		//Other Parsers
		/*
		 * InputParser parser = new WriterParser(io); //Debug Parser
		 */
		 
		//Set the chosen Parser
		io.setParser(parser);
	
	}
	
	//Method for checking, wethere the Acitivity should change
	public void ioInterruption()
	{
		if(parser.getNewActivity() != null)
		{
			Intent intent = new Intent(this, parser.getNewActivity());
			startActivity(intent);
		}
	}


}

