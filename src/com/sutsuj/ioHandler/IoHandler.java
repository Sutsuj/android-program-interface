package com.sutsuj.ioHandler;

import android.widget.*;
import android.view.*;
import android.util.*;
import android.app.*;

import com.sutsuj.ioHandler.parser.InputParser;
import com.sutsuj.GUI.*;

/*
 * abstract IoHandler for other IoHandles.
 * This Handler gives the most important functions.
 * Their childs are just for chosing the result of this IoHandler
 */

abstract public class IoHandler
{
	private MainActivity main;
	private InputParser parser;
	private TextView output;
	private EditText input;
	private String cache;
	private boolean isCacheSetted;

	//Get importend Elements from the GUI
	public IoHandler(MainActivity pMain, TextView pOut, EditText pIn) throws NullPointerException
	{
		if(output == null || input == null)
			throw new NullPointerException();
		this.main = pMain;
		output = pOut;
		input = pIn;
		input.setOnKeyListener(new editTextKeyListener(this));
	}
	
	//method for printing strings
	public void print(String pOutput)
	{
		this.output.append("\n" + pOutput);
	}
	
	
	/*
	 * The following methods are for input handling
	 *
	 */
	 
	//Interface to get the intput
	public String getInput()
	{
		while(!isCacheSetted()){} //wait for input
		return cache;
	}
	 
	//Method for waiting, that the input is setted
	synchronized private boolean isCacheSetted()
	{
		if(isCacheSetted)
		{
			isCacheSetted = false;
			return true;
		}
		else
			return false;
	}
	
	//Method for setting the input
	synchronized private void handleInput()
	{
		cache = input.getText().toString();
		input.getText().clear();
		isCacheSetted = true;
		main.ioInterruption();
	}
	
	//method for reading with different results
	abstract protected Object read();
	
	//Listener for handleInput
	public class editTextKeyListener implements View.OnKeyListener
	{	
	
		IoHandler master;
		
		public editTextKeyListener(IoHandler master)
		{
			this.master = master;
		}
		
		@Override
		public boolean onKey(View v, int keyCode, KeyEvent event)
		{
			if(event.getAction() == KeyEvent.ACTION_DOWN)
			{
				switch(keyCode)
				{
					//Set input, when enter is pressed
					case KeyEvent.KEYCODE_DPAD_CENTER:
					case KeyEvent.KEYCODE_ENTER:
						master.handleInput();
						return true;
					default:
					break;
				}
			}	
			return false;
		}
	}
}





