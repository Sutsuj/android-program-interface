package com.sutsuj.ioHandler;

import android.widget.*;
import com.sutsuj.GUI.*;
import com.sutsuj.ioHandler.parser.InputParser;

/* IoHandler wchich supports Parser implemented as InputParser
 * Doesn't make result on read
 */
public class ParseIoHandler extends IoHandler
{
	private InputParser parser;

	//Constructor for setting parser
	public ParseIoHandler(MainActivity pMain, TextView pOut, EditText pIn, InputParser pParser) throws NullPointerException
	{
		super(pMain, pOut, pIn);
		setParser(pParser);
	}

	//Second method for presetting the parser
	public void setParser(InputParser pParser)
	{
		parser = pParser;
	}

	/* Read method
	 * does, what the parser want to
	 * result: everytime null
	 */
	@Override
	public Object read()
	{
		parser.pars(getInput());
		return null;
	}	
	
}





