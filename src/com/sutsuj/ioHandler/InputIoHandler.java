package com.sutsuj.ioHandler;

import android.widget.*;
import com.sutsuj.GUI.*;

/*
 * Handy Io Handler for Input with String result and printer
 */
public class InputIoHandler extends IoHandler
{
	
	public InputIoHandler(MainActivity pMain, TextView pOut, EditText pIn) throws NullPointerException
	{
		super(pMain, pOut, pIn);
	}

	/* read method
	 * result: the string entered
	 */
	@Override
	public String read()
	{
		return getInput();
	}
}
