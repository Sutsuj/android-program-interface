package com.sutsuj.ioHandler.parser;

import android.content.*;

import com.sutsuj.ioHandler.*;

/* Abstract method for Parsers, that can be used by ParseIoHandler
 * Requiers the method pars
 * getNewActivity is for setting new Activitys
 */
abstract class InputParser
{
	
	protected IoHandler io;
	protected Class<?> newActivity;
	
	public InputParser(IoHandler pIo)
	{
		io = pIo;
		newActivity = null;
	}
	
	protected IoHandler getIoHandler()
	{
		return io;
	}
	
	//Do something with the input String
	abstract public void pars(String input);
	
	//Method for mainfunction, to set new Activity
	public Class<?> getNewActivity()
	{
		return this.newActivity;
	}
}
