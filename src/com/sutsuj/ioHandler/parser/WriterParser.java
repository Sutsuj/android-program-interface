package com.sutsuj.ioHandler.parser;

import com.sutsuj.ioHandler.*;

/* Parser for debugging
 * Prints everytime what you have been written in.
 */
public class WriterParser extends InputParser
{

	public WriterParser(IoHandler io)
	{
		super(io);
	}

	//Just print the input
	public void pars(String input)
	{
		getIoHandler().print(input);
	}
}
