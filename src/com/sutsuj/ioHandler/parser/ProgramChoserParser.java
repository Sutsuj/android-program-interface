package com.sutsuj.ioHandler.parser;

import java.util.*;
import android.content.*;
import com.sutsuj.ioHandler.*;
import com.sutsuj.kochkurve.Kochkurve;
import com.sutsuj.treetest.*;

/* Method for chosing Programs in starthub
 * can set new Activitys or start new Programs in pars()
 */
public class ProgrammChoserParser extends InputParser
{
	
	public ProgrammChoserParser(IoHandler pIo)
	{

		super(pIo);
	}
	
	//Sets the expexted programs for the input
	@Override
	public void pars(String input)
	{
		newActivity = null;
		switch(input)
		{
			case "HelloWorld":
				io.print("Hello!");
				break;
			default:
				io.print("No program found.");
			break;
		}
	}
}
